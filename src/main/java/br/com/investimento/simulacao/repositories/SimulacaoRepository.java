package br.com.investimento.simulacao.repositories;

import br.com.investimento.simulacao.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer>{
}
