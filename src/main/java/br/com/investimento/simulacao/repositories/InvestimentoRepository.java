package br.com.investimento.simulacao.repositories;

import br.com.investimento.simulacao.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
