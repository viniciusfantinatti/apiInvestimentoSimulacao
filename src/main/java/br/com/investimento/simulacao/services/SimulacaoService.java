package br.com.investimento.simulacao.services;

import br.com.investimento.simulacao.models.Simulacao;
import br.com.investimento.simulacao.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;


    //INCLUSÃO
    public void incluirSimulacao(Simulacao simulacao){
        simulacaoRepository.save(simulacao);
    }

    //CONSULTA
    public Iterable<Simulacao> consultarTodasSimulacao(){
        Iterable<Simulacao> objSimulacao = simulacaoRepository.findAll();
        return objSimulacao;
    }

}
