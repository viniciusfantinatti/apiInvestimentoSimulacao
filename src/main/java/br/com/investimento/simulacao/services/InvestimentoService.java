package br.com.investimento.simulacao.services;

import br.com.investimento.simulacao.DTOs.RespostaDTO;
import br.com.investimento.simulacao.models.Investimento;
import br.com.investimento.simulacao.models.Simulacao;
import br.com.investimento.simulacao.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    //INCLUSÃO
    public Investimento incluirInvestimento(Investimento investimento){
        Investimento objInvestimento = investimentoRepository.save(investimento);
        return objInvestimento;
    }

    //CONSULTAS
    public Investimento consultarInvestimentoPorId(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        }
        throw new RuntimeException("O Investimento não foi encontrado");
    }

    public Iterable<Investimento> consultarTodosInvestimentos(){
        Iterable<Investimento> objInvestimento = investimentoRepository.findAll();
        return objInvestimento;
    }

    //ALTERAÇÃO
    public Investimento alterarInvestimento(int id, Investimento investimento){
        if(investimentoRepository.existsById(id)){
            investimento.setId(id);
            Investimento objInvestimento = investimentoRepository.save(investimento);
            return objInvestimento;
        }
        throw new RuntimeException("O Investimento não foi encontrado.");
    }

    //DELEÇÃO
    public void deletarInvestimento(int id){
        if (investimentoRepository.existsById(id)){
            investimentoRepository.deleteById(id);
        }else{
            throw new  RuntimeException("O Investimento não existe.");
        }
    }

    //CRIAR SIMULACAO PARA O INVESTIMENTO
    public RespostaDTO fazerSimulacao(int id, Simulacao simulacao){

        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (!optionalInvestimento.isPresent()){
            throw new RuntimeException("O Investimento não foi encontrado");
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setRendimentoPorMes(optionalInvestimento.get().getRendimentoMes());
        double montante = simulacao.getValorAplicado() *
                (1 + Math.pow(optionalInvestimento.get().getRendimentoMes(),simulacao.getQuantidadeMeses()));
        respostaDTO.setMontante(montante);
        simulacao.setInvestimento(optionalInvestimento.get());
        simulacaoService.incluirSimulacao(simulacao);
        return respostaDTO;
    }
}
