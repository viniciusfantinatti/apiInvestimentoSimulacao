package br.com.investimento.simulacao.controllers;

import br.com.investimento.simulacao.models.Simulacao;
import br.com.investimento.simulacao.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    //CONSULTA
    @GetMapping
    public Iterable<Simulacao> consultarTodasSimulacoes(){
        Iterable<Simulacao> objSimulacao = simulacaoService.consultarTodasSimulacao();
        return  objSimulacao;
    }
}
