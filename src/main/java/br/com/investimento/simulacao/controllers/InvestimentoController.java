package br.com.investimento.simulacao.controllers;

import br.com.investimento.simulacao.DTOs.RespostaDTO;
import br.com.investimento.simulacao.models.Investimento;
import br.com.investimento.simulacao.models.Simulacao;
import br.com.investimento.simulacao.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    //INCLUSÃO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento incluirInvestimento(@RequestBody @Valid Investimento investimento){
        return investimentoService.incluirInvestimento(investimento);
    }

    @PostMapping("/{id}/simulacao")
    public ResponseEntity<RespostaDTO> incluirSimulacao(@PathVariable("id") int id,
                                                        @RequestBody @Valid Simulacao simulacao){

        try {
            RespostaDTO objRespostaDTO = investimentoService.fazerSimulacao(id,simulacao);
            return ResponseEntity.status(HttpStatus.CREATED).body(objRespostaDTO);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


    //CONSULTAS
    @GetMapping
    public Iterable<Investimento> consultarTodosInvestimento(){
        Iterable<Investimento> objInvestimento = investimentoService.consultarTodosInvestimentos();
        return objInvestimento;
    }

    @GetMapping("/{id}")
    public Investimento consultarPorId(@PathVariable("id") int id){
        try {
            Investimento investimento = investimentoService.consultarInvestimentoPorId(id);
            return investimento;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


    //ALTERAÇÃO
    @PutMapping("/{id}")
    public Investimento alterarInvestimento(@PathVariable("id") int id, @RequestBody @Valid Investimento investimento){
        try {
            Investimento objInvestimento = investimentoService.alterarInvestimento(id, investimento);
            return objInvestimento;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


    //DELEÇÃO
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarInvestimento(@PathVariable("id") int id){
        try {
            investimentoService.deletarInvestimento(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
