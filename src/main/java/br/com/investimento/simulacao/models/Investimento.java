package br.com.investimento.simulacao.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome do Investimento não pode ser Nulo.")
    @NotBlank(message = "Nome do Investimento não pode estar em Branco.")
    @Size(min = 3, max = 50, message = "Nome do Investimento deve ter entre 3 e 100 caracteres.")
    @Column(length = 100,unique = true)
    private String nome;


    @Column(precision = 5, scale = 2)
    private double rendimentoMes;

    public Investimento() {
    }

    public Investimento(String nome, double rendimentoMes) {
        this.nome = nome;
        this.rendimentoMes = rendimentoMes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoMes() {
        return rendimentoMes;
    }

    public void setRendimentoMes(double rendimentoMes) {
        this.rendimentoMes = rendimentoMes;
    }
}
