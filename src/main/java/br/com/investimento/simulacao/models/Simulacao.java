package br.com.investimento.simulacao.models;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 3, max = 100, message = "O nome deve ter no entre 3 e 100 caracteres.")
    @NotBlank(message = "O nome não pode estar em branco.")
    @NotNull(message = "O nome não pode ser nulo.")
    @Column(length = 100)
    private String nomeInteressado;

    @Email
    @Column(length = 100)
    private String email;

    @Column(length = 15, precision = 15, scale = 2)
    private double valorAplicado;

    @Column(length = 2)
    private int quantidadeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    public Investimento investimento;


    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
