package br.com.investimento.simulacao.DTOs;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RespostaDTO {

    private double rendimentoPorMes;
    private double montante;

    public RespostaDTO() {
    }

    public RespostaDTO(double rendimentoPorMes, double montante) {
        this.rendimentoPorMes = rendimentoPorMes;
        this.montante = montante;
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        BigDecimal bigDecimalMontante = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bigDecimalMontante.doubleValue();
    }
}
