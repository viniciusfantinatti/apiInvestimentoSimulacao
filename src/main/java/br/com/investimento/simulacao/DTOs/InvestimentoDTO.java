package br.com.investimento.simulacao.DTOs;

import br.com.investimento.simulacao.models.Investimento;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class InvestimentoDTO {

    private int id;

    @NotNull(message = "Nome do Investimento não pode ser Nulo.")
    @NotBlank(message = "Nome do Investimento não pode estar em Branco.")
    @Size(min = 3, max = 50, message = "Nome do Investimento deve ter entre 3 e 50 caracteres.")
    private String nome;

    private double rendimentoMes;

    public InvestimentoDTO() {
    }

//    public InvestimentoDTO(int id, @NotNull(message = "Nome do Investimento não pode ser Nulo.")
//                           @NotBlank(message = "Nome do Investimento não pode estar em Branco.")
//    @Size(min = 3, max = 50, message = "Nome do Investimento deve ter entre 3 e 50 caracteres.") String nome,
//                           double rendimentoMes) {
//        this.id = id;
//        this.nome = nome;
//        this.rendimentoMes = rendimentoMes;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoMes() {
        return rendimentoMes;
    }

    public void setRendimentoMes(double rendimentoMes) {
        this.rendimentoMes = rendimentoMes;
    }

    public Investimento transformaParaObjInvestimento(){
        return new Investimento(nome, rendimentoMes);
    }
}
