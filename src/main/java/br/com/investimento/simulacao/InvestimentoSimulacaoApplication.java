package br.com.investimento.simulacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestimentoSimulacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvestimentoSimulacaoApplication.class, args);
	}

}
